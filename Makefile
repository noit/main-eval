.PHONY: all clean
CC = gcc
CFLAGS = -O2 -Wall -Werror -std=c11
LDFLAGS =
SOURCES = $(wildcard *.c)
EXECUTABLES = $(SOURCES:.c=)

$(EXECUTABLES): $(SOURCES)
	$(CC) $(CFLAGS) $(LDFLAGS) $@.c -o $@

all: $(EXECUTABLES)

clean:
	rm -rf $(EXECUTABLES) $(OBJECTS)
