# Description

Basic arithmetic evaluator using only the main function. If no binary operator provided assumes multiplication. '+' and '-' are also unitary.

# Example

```bash
$ cat tests/tests.23
-1 + (2 / 3) + +++(4 + 4 + 4 * 5 - (3 3 (3 3))(4 (4--4) 4)(5   /   -+-+-+-5*5 (---5*--5))) + 3  4  5 / 4 / 5 ------ 100 (----(---100)) * --10 - ( ( ( ( ((4))   ))) ) 3
$ ./main < tests/test.23
1196018
```
