#include <stdio.h>
#include <stdlib.h>

#define ERROR_IF(cnd, ...) if (cnd) { fprintf(stderr, __VA_ARGS__); exit(-1); }

int main(int argc, char* argv[]) {
    char c, op_next = 0, div = 0;
    int n, total = 0, acum = 1;
    while (scanf(" %c", &c) != EOF) {
        switch (c) {
            case '+':
            case '-':
                if (op_next) {
                    total += acum;
                    acum = 1;
                    op_next = 0;
                }
                acum *= ',' - c;
                continue;
            case '/':
                div = 1;
            case '*':
                ERROR_IF(!op_next, "Can't have '%c' as unitary operator\n", c);
                op_next = 0;
                continue;
            case ')':
                ERROR_IF(!op_next, "Operator before ')' or empty\n");
                ERROR_IF(argv, "Unmatched ')'\n");
                return total + acum;
            case '(':
                n = main(0, NULL);
                break;
            default:
                ERROR_IF(c < '0' || c > '9', "Invalid operator '%c'\n", c);
                ungetc(c, stdin);
                scanf("%u", &n);
        }
        if (div) {
            ERROR_IF(!n, "Division by zero\n");
            acum /= n;
        } else
            acum *= n;
        div = 0;
        op_next = 1;
    }
    ERROR_IF(!op_next, "Operator before the end or empty\n");
    ERROR_IF(!argv, "Unmatched '('\n");
    printf("%i\n", total + acum);
}
